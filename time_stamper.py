import requests
import json
import base64
import time

TIME_STAMP_URL_SERVER = "https://fw2.bry.com.br/api/carimbo-service/v1/timestamps"

HEADER = {'Content-Type': 'application/json', 'Authorization': 'Bearer insert-a-valid-token'}

# Step C1 - Add the file path to be stamped 
file = open("./document/image.png", 'rb').read()
b64_file = base64.b64encode(file) 
decoded_b64 = b64_file.decode("utf-8") 

def token_verify():
    aux_authorization = HEADER['Authorization'].split(' ')
    if(aux_authorization[1] == 'insert-a-valid-token'):
        print("Configure a valid token")
        return False
    else:
        return True

def time_stamp_content():

    print("================ Initializing document time-stamp realization ... ================")
    print("")

    time_stamp_form = {
        'nonce': 1,
        'hashAlgorithm': 'SHA256',
        'format': 'FILE',
        "documents":[{"nonce":1,"content": decoded_b64}]
    }

    # Step C2 - Performs the time stamp generation process
    response = requests.post(TIME_STAMP_URL_SERVER, data = json.dumps(time_stamp_form), headers=HEADER)
    if response.status_code == 200:
        data = response.json()

        print("Quantity of time-stamp completed: ", len(time_stamp_form["documents"]))
        print("")
        print("Nonce of time-stamp: ", data['nonce'])
        print("")
        print("Base64 encoded time-stamp: ", data['timeStamps'][0]['content'])
    else:
        print(response.text)


def time_stamp_hash_content():

    print("")
    print("============ Starting document hash time-stamping ... ============")
    print("")

    time_stamp_form = {
        'nonce': 1,
        'hashAlgorithm': 'SHA256',
        'format': 'HASH',
        # Step H1 - Add the hash to be stamped in the "content" field
        "documents":[{"nonce":1,"content": "FC30043F87BC8A9B9DEB9F663882C4652A34AD8E53D2A74317F9CD4D8CFB33D0"}]
    }
    # Step H2 - Performs the time stamp generation process
    response = requests.post(TIME_STAMP_URL_SERVER, data = json.dumps(time_stamp_form), headers=HEADER)
    if response.status_code == 200:
        data = response.json()

        print("Quantity of time-stamp completed: ", len(time_stamp_form["documents"]))
        print("")
        print("Nonce of time-stamp: ", data['nonce'])
        print("")
        print("Base64 encoded time-stamp: ", data['timeStamps'][0]['content'])
        print("")

    else:
        print(response.text)


if(token_verify()):
    time_stamp_content()
    time.sleep(2)
    time_stamp_hash_content()
